package com.projectlikes.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.projectlikes.Adapters.AboutUsAdapter;
import com.projectlikes.Modal.AboutusModal;
import com.projectlikes.R;

import java.util.ArrayList;

public class AboutUsActivity extends AppCompatActivity {
    AboutUsAdapter aboutUsAdapter;
    RecyclerView.LayoutManager layoutManager;
    ArrayList<AboutusModal>modalArrayList=new ArrayList<>();
    RecyclerView rec_about;
    ImageView img_back;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us);
        rec_about=findViewById(R.id.rec_about);
        layoutManager=new LinearLayoutManager(AboutUsActivity.this,RecyclerView.VERTICAL,false);
        rec_about.setLayoutManager(layoutManager);
         rec_about.setHasFixedSize(true);
        aboutUsAdapter=new AboutUsAdapter(AboutUsActivity.this,modalArrayList);
        rec_about.setAdapter(aboutUsAdapter);
        img_back=findViewById(R.id.img_back);
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();

            }
        });

        show_about();
    }

    public void show_about(){

        AboutusModal modal=new AboutusModal(R.drawable.youtube_subscribe_banner,"About Project Likes","");
        for (int i=0;i<1;i++){
            modalArrayList.add(modal);
        }
    }
}