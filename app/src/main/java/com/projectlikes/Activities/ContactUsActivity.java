package com.projectlikes.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.projectlikes.R;
import com.projectlikes.Utils.AppsContants;
import com.projectlikes.Utils.GPSTracker;
import com.projectlikes.Utils.HTTPCALL;

import org.json.JSONObject;

public class ContactUsActivity extends AppCompatActivity implements OnMapReadyCallback {
    ImageView img_back;
    private GoogleMap mMap;
    GPSTracker gpsTracker;
    Marker mMarker;
    double latitude;
    double longtitude;
    LatLng latLng;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);
        img_back = findViewById(R.id.img_back);
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();

            }
        });

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        gpsTracker = new GPSTracker(this);

        latitude = gpsTracker.getLatitude();
        longtitude = gpsTracker.getLongitude();
        Log.e("yhcuvgf", latitude + "");
        Log.e("yhcxcuvgf", longtitude + "");


        final EditText UserName = findViewById(R.id.UserName);
        final EditText emailUserName = findViewById(R.id.emailUserName);
        final EditText edt_subject = findViewById(R.id.edt_subject);
        final EditText edt_msg = findViewById(R.id.edt_msg);
        Button btn_send = findViewById(R.id.btn_send);

        AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        final String strUserId = AppsContants.sharedpreferences.getString(AppsContants.UserId, "");

        btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String strName=UserName.getText().toString();
                String strEmail=emailUserName.getText().toString();
                String strSubject=edt_subject.getText().toString();
                String strMessage=edt_msg.getText().toString();
                ContactUS(strUserId,strName,strEmail,strSubject,strMessage);
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        ///Show Cureent Location on map
        if (ActivityCompat.checkSelfPermission(getApplication(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplication(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            return;
        }

        latLng = new LatLng(latitude, longtitude);
        mMap.setMyLocationEnabled(true);
        mMap.setTrafficEnabled(false);
        ////set direction circle marker bootom right on map(Circle)
        mMap.setPadding(10, 420, 10, 10);


        ////SHOW LOCATION OF MARKER AND COLOUR
        mMap.addMarker(new MarkerOptions()
                .position(latLng)
                .title("My position")
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));

        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));


    }


    public void ContactUS(String strUserId, String strName, String strEmail, String strSubject, String strMessage) {

        final ProgressDialog progressDialog = new ProgressDialog(ContactUsActivity.this);
        progressDialog.setMessage("Please wait..");
        progressDialog.setIndeterminate(true);
        progressDialog.show();
        progressDialog.setCancelable(false);

        AndroidNetworking.post(HTTPCALL.contact_us)
                .addBodyParameter("name", strName)
                .addBodyParameter("email", strEmail)
                .addBodyParameter("subject", strSubject)
                .addBodyParameter("message", strMessage)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {

                            Log.e("response", response.toString());
                            if (response.getString("result").equals("Successfully")) {
                                progressDialog.dismiss();
                                Toast.makeText(ContactUsActivity.this, "Request Submitted", Toast.LENGTH_SHORT).show();
                                finish();
                            } else {
                                progressDialog.dismiss();
                                Toast.makeText(ContactUsActivity.this, response.getString("result"), Toast.LENGTH_SHORT).show();
                            }

                        } catch (Exception ex) {
                            progressDialog.dismiss();
                            Log.e("Catch-Block", ex.toString());
                        }
                    }

                    @Override
                    public void onError(ANError anError) {

                        progressDialog.dismiss();
                        Log.e("onError", "Error" + anError.getMessage());
                    }
                });
    }
}