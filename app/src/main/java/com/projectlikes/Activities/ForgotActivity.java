package com.projectlikes.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.projectlikes.MainActivity;
import com.projectlikes.R;
import com.projectlikes.Utils.AppsContants;
import com.projectlikes.Utils.HTTPCALL;

import org.json.JSONObject;

public class ForgotActivity extends AppCompatActivity {

    EditText emailUserName;
    Button btn_submit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot);

        emailUserName = findViewById(R.id.emailUserName);
        btn_submit = findViewById(R.id.btn_submit);

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String strEmail=emailUserName.getText().toString();
                if(strEmail.equals("")){

                    Toast.makeText(ForgotActivity.this, R.string.enter_email, Toast.LENGTH_SHORT).show();
                }
                else {

                    Forgot(strEmail);
                }






            }
        });
    }


    public void Forgot(String strEditEmail) {

        final ProgressDialog progressDialog = new ProgressDialog(ForgotActivity.this);
        progressDialog.setMessage(getResources().getString(R.string.wait));
        progressDialog.setIndeterminate(true);
        progressDialog.show();
        progressDialog.setCancelable(false);

        AndroidNetworking.post(HTTPCALL.forgot)
                .addBodyParameter("email", strEditEmail)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {

                            Log.e("response", response.toString());
                            if (response.getString("result").equals("Please Check Your Email To Get Your Password")) {

                                Toast.makeText(ForgotActivity.this, R.string.forget_message, Toast.LENGTH_SHORT).show();
                                finish();
                            }
                            else {

                                progressDialog.dismiss();
                                Toast.makeText(ForgotActivity.this, response.getString("result"), Toast.LENGTH_SHORT).show();
                            }

                        } catch (Exception ex) {
                            progressDialog.dismiss();
                            Log.e("Catch-Block", ex.toString());
                        }
                    }
                    @Override
                    public void onError(ANError anError) {

                        progressDialog.dismiss();
                        Log.e("onError", "Error" + anError.getMessage());
                    }
                });
    }
}