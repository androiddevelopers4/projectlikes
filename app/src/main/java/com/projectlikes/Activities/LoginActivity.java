package com.projectlikes.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.projectlikes.MainActivity;
import com.projectlikes.R;
import com.projectlikes.Utils.AppsContants;
import com.projectlikes.Utils.HTTPCALL;

import org.json.JSONObject;

public class LoginActivity extends AppCompatActivity {
    TextView txt_signUp, txtForgot;
    Button btn_signUp;
    EditText emailUserName;
    EditText ed_pass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        txt_signUp = findViewById(R.id.txtAlreadyHaveAccount);
        txtForgot = findViewById(R.id.txtForgot);
        btn_signUp = findViewById(R.id.btn_signUp);
        txt_signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LoginActivity.this, RegisterActivity.class));

            }
        });
        txtForgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LoginActivity.this, ForgotActivity.class));

            }
        });


        emailUserName = findViewById(R.id.emailUserName);
        ed_pass = findViewById(R.id.ed_pass);

        btn_signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                String strEditEmail = emailUserName.getText().toString().trim();
                String strPassword = ed_pass.getText().toString().trim();

                 if (strEditEmail.equals("")) {
                    Toast.makeText(LoginActivity.this, R.string.enter_email, Toast.LENGTH_SHORT).show();
                } else if (strPassword.equals("")) {
                    Toast.makeText(LoginActivity.this, R.string.enter_password, Toast.LENGTH_SHORT).show();
                } else {
                    Login(strEditEmail,strPassword);
                    //  startActivity(new Intent(Signup.this,VerifyOtpActivity.class));
                }


            }
        });



    }



    public void Login(String strEditEmail, String strPassword) {

        final ProgressDialog progressDialog = new ProgressDialog(LoginActivity.this);
        progressDialog.setMessage(getResources().getString(R.string.wait));
        progressDialog.setIndeterminate(true);
        progressDialog.show();
        progressDialog.setCancelable(false);

        AndroidNetworking.post(HTTPCALL.login)
                .addBodyParameter("email", strEditEmail)
                .addBodyParameter("password", strPassword)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {

                            Log.e("response", response.toString());
                            if (response.getString("result").equals("Successfully Login")) {

                                AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
                                editor.putString(AppsContants.UserId, response.getString("id"));
                                editor.putString(AppsContants.fname, response.getString("fname"));
                                editor.putString(AppsContants.lname, response.getString("lname"));
                                editor.putString(AppsContants.email, response.getString("email"));
                                editor.putString(AppsContants.gender, response.getString("gender"));
                                editor.putString(AppsContants.phone, response.getString("phone"));
                                editor.putString(AppsContants.rafral_id, response.getString("rafral_id"));
                                editor.putString(AppsContants.image, response.getString("image"));
                                editor.commit();
                                progressDialog.dismiss();
                                startActivity(new Intent(LoginActivity.this, MainActivity.class));
                                finish();

                            } else {
                                progressDialog.dismiss();
                                Toast.makeText(LoginActivity.this, response.getString("result"), Toast.LENGTH_SHORT).show();
                            }

                        } catch (Exception ex) {
                            progressDialog.dismiss();
                            Log.e("Catch-Block", ex.toString());
                        }
                    }

                    @Override
                    public void onError(ANError anError) {

                        progressDialog.dismiss();
                        Log.e("onError", "Error" + anError.getMessage());
                    }
                });
    }
}