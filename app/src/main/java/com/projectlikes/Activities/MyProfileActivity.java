package com.projectlikes.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.bumptech.glide.Glide;
import com.projectlikes.MainActivity;
import com.projectlikes.R;
import com.projectlikes.Utils.AppsContants;
import com.projectlikes.Utils.HTTPCALL;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;

import de.hdodenhof.circleimageview.CircleImageView;

public class MyProfileActivity extends AppCompatActivity {
    CircleImageView profile_Img;
    ImageView img_Back;
    Button btn_Update;
    File file;
    private static final String IMAGE_DIRECTORY = "/directory";
    private int GALLERY = 1, CAMERA = 2;
    EditText edt_name;
    EditText et_email;
    EditText edt_Lastname;
    TextView txtFreeFollowers;
    String strFile="";
    String refId="";
    TextView txtRefId;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_profile);


        txtRefId=findViewById(R.id.txtRefId);
        txtFreeFollowers=findViewById(R.id.txtFreeFollowers);
        img_Back=findViewById(R.id.img_Back);
        img_Back.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        } );

        btn_Update=findViewById(R.id.btn_Update);
        btn_Update.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {



            }
        } );

        profile_Img=findViewById(R.id.profile_Img);
        profile_Img.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showPictureDialog();

            }
        } );

        edt_name=findViewById(R.id.edt_name);
        et_email=findViewById(R.id.et_email);
        edt_Lastname=findViewById(R.id.edt_Lastname);



        AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        final String strUserId = AppsContants.sharedpreferences.getString(AppsContants.UserId, "");
        String strImage = AppsContants.sharedpreferences.getString(AppsContants.image, "");
        String strPath = AppsContants.sharedpreferences.getString(AppsContants.path, "");
        edt_name.setText(AppsContants.sharedpreferences.getString(AppsContants.fname, ""));
        edt_Lastname.setText(AppsContants.sharedpreferences.getString(AppsContants.lname, ""));
        et_email.setText(AppsContants.sharedpreferences.getString(AppsContants.email, ""));
        Log.e("sdfsdsdfsf",strUserId);

        try {
            Glide.with(MyProfileActivity.this)
                    .load(strPath+strImage)
                    .thumbnail(Glide.with(MyProfileActivity.this).load(R.drawable.ezgifresize))
                    .into(profile_Img);
        } catch (Exception e) {
            e.printStackTrace();
        }

        btn_Update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String strName=edt_name.getText().toString();
                String strEmail=et_email.getText().toString();
                String strLastName=edt_Lastname.getText().toString();
                Update(strUserId,strName,strEmail,strLastName);
            }
        });

        Showprofile(strUserId);


        txtRefId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                Intent share = new Intent(android.content.Intent.ACTION_SEND);
                share.setType("text/plain");
                share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);

                share.putExtra(Intent.EXTRA_SUBJECT, "Referal Code"+" "+"12465");
                share.putExtra(Intent.EXTRA_TEXT, "https://play.google.com/store/apps/details?id=in.startv.hotstar");
                startActivity(Intent.createChooser(share, "Share text to..."));


            }
        });
    }

    /////////*PROFILE IMAGE UPLOAD WORK IN FRAGMENT*//////////
    public void showPictureDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.select_action);
        String[] pictureDialogItems = {getString(R.string.choose_from_gallery),getString(R.string.take_photo) ,getString(R.string.cancel)};

        builder.setItems(pictureDialogItems, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                switch (which) {

                    case 0:
                        choosePhotoFromGallery();
                        break;

                    case 1:
                        captureFromCamera();
                        break;
                }

            }
        });

        builder.show();
    }


    public void choosePhotoFromGallery() {

        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(intent, GALLERY);
    }


    public void captureFromCamera() {

        Intent intent_2 = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent_2, CAMERA);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == this.RESULT_CANCELED) {
            return;
        }
        if (requestCode == GALLERY) {
            if (data != null) {
                Uri contentURI = data.getData();
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), contentURI);
                    saveImage(bitmap);
                    Toast.makeText(getApplicationContext(), "Image Saved!", Toast.LENGTH_SHORT).show();
                    profile_Img.setImageBitmap(bitmap);

                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Failed!", Toast.LENGTH_SHORT).show();
                }
            }

        } else if (requestCode == CAMERA) {
            Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
            profile_Img.setImageBitmap(thumbnail);
            saveImage(thumbnail);
            Toast.makeText(getApplicationContext(), "Image Saved!", Toast.LENGTH_SHORT).show();
        }
    }

    public String saveImage(Bitmap myBitmap) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        File wallpaperDirectory = new File(
                Environment.getExternalStorageDirectory() + IMAGE_DIRECTORY);

        if (!wallpaperDirectory.exists()) {
            wallpaperDirectory.mkdirs();
        }

        try {
            file = new File(wallpaperDirectory, Calendar.getInstance()
                    .getTimeInMillis() + ".jpg");
            file.createNewFile();
            FileOutputStream fo = new FileOutputStream(file);
            fo.write(bytes.toByteArray());
            MediaScannerConnection.scanFile(getApplicationContext(),
                    new String[]{file.getPath()},
                    new String[]{"image/jpeg"}, null);
            fo.close();
            Log.d("TAG", "File Saved::---&gt;" + file.getAbsolutePath());
            Log.e("sdfsadfsdfsdfffsdff",file.toString());
             strFile=file.toString();

            return file.getAbsolutePath();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        return "";
    }

    public void Update(String strUserId, String strName, String strEmail, String strLastName) {
        final ProgressDialog progressDialog = new ProgressDialog(MyProfileActivity.this);

        progressDialog.setMessage(getString(R.string.wait));
        progressDialog.setIndeterminate(true);
        progressDialog.show();
        progressDialog.setCancelable(false);

        if(strFile.equals("")){

            AndroidNetworking.post(HTTPCALL.update_profile)
                    .addBodyParameter("user_id", strUserId)
                    .addBodyParameter("fname", strName)
                    .addBodyParameter("lname", strLastName)
                    .addBodyParameter("email", strEmail)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                Log.e("response", response.toString());

                                if (response.getString("result").equals("successfully")) {

                                    progressDialog.dismiss();
                                    AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                                    SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
                                    editor.putString(AppsContants.fname, response.getString("fname"));
                                    editor.putString(AppsContants.lname, response.getString("lname"));
                                    editor.putString(AppsContants.email, response.getString("email"));
                                    editor.putString(AppsContants.image, response.getString("image"));
                                    editor.putString(AppsContants.path, response.getString("path"));
                                    editor.commit();
                                    Toast.makeText(MyProfileActivity.this, R.string.profile_updated, Toast.LENGTH_SHORT).show();
                                    finish();
                                }
                                else {

                                    progressDialog.dismiss();
                                    Toast.makeText(MyProfileActivity.this, response.getString("result"), Toast.LENGTH_SHORT).show();
                                }

                            } catch (Exception ex) {
                                progressDialog.dismiss();
                                Log.e("Catch-Block", ex.toString());
                            }
                        }

                        @Override
                        public void onError(ANError anError) {

                            progressDialog.dismiss();
                            Log.e("onError", "Error" + anError.getMessage());
                        }
                    });

        }
        else {
            Log.e("dfgdgdgdg",file.toString());
            AndroidNetworking.upload(HTTPCALL.update_profile)
                    .addMultipartParameter("user_id", strUserId)
                    .addMultipartParameter("fname", strName)
                    .addMultipartParameter("lname", strLastName)
                    .addMultipartParameter("email", strEmail)
                    .addMultipartFile("image", file)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {

                                Log.e("response", response.toString());
                                if (response.getString("result").equals("successfully")) {
                                    progressDialog.dismiss();

                                    AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                                    SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
                                    editor.putString(AppsContants.fname, response.getString("fname"));
                                    editor.putString(AppsContants.lname, response.getString("lname"));
                                    editor.putString(AppsContants.email, response.getString("email"));
                                    editor.putString(AppsContants.image, response.getString("image"));
                                    editor.putString(AppsContants.path, response.getString("path"));
                                    editor.commit();

                                    Toast.makeText(MyProfileActivity.this, "Profile Updated", Toast.LENGTH_SHORT).show();
                                    finish();
                                } else {
                                    progressDialog.dismiss();
                                    Toast.makeText(MyProfileActivity.this, response.getString("result"), Toast.LENGTH_SHORT).show();
                                }

                            } catch (Exception ex) {
                                progressDialog.dismiss();
                                Log.e("Catch-Block", ex.toString());
                            }
                        }

                        @Override
                        public void onError(ANError anError) {

                            progressDialog.dismiss();
                            Log.e("onError", "Error" + anError.getMessage());
                        }
                    });
        }


    }


    public void Showprofile(String strUserId){

            final ProgressDialog progressDialog = new ProgressDialog(MyProfileActivity.this);
            progressDialog.setMessage("Please wait..");
            progressDialog.setIndeterminate(true);
            progressDialog.show();
            progressDialog.setCancelable(false);

                AndroidNetworking.post(HTTPCALL.showprofile)
                        .addBodyParameter("user_id", strUserId)
                        .build()
                        .getAsJSONObject(new JSONObjectRequestListener() {
                            @Override
                            public void onResponse(JSONObject response) {
                                try {
                                    Log.e("response", response.toString());

                                    if (response.has("id")) {

                                        progressDialog.dismiss();
                                        txtFreeFollowers.setText(response.getString("gif_follower")+" "+"Followers");
                                        txtRefId.setText("Referal Id"+" "+response.getString("rafral_id"));
                                        refId=response.getString("rafral_id");
                                    }
                                    else {
                                        progressDialog.dismiss();
                                        Toast.makeText(MyProfileActivity.this, response.getString("result"), Toast.LENGTH_SHORT).show();
                                    }

                                } catch (Exception ex) {
                                    progressDialog.dismiss();
                                    Log.e("Catch-Block", ex.toString());
                                }
                            }

                            @Override
                            public void onError(ANError anError) {

                                progressDialog.dismiss();
                                Log.e("onError", "Error" + anError.getMessage());
                            }
                        });


    }
}