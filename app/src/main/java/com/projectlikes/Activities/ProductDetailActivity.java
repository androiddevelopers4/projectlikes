package com.projectlikes.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.text.style.IconMarginSpan;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.bumptech.glide.Glide;
import com.projectlikes.Adapters.ProductHeaderAdapter;
import com.projectlikes.MainActivity;
import com.projectlikes.R;
import com.projectlikes.Utils.AppsContants;
import com.projectlikes.Utils.HTTPCALL;

import org.json.JSONObject;

public class ProductDetailActivity extends AppCompatActivity {
    ImageView img_back, imageAnim, imgProduct;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_detail);
        img_back = findViewById(R.id.img_back);
        imageAnim = findViewById(R.id.imageAnim);
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();

            }
        });

        AnimationDrawable animation = new AnimationDrawable();
        animation.addFrame(ProductDetailActivity.this.getResources().getDrawable(R.drawable.cicle_blue), 100);
        animation.addFrame(ProductDetailActivity.this.getResources().getDrawable(R.drawable.cicle_yellow), 500);
        animation.addFrame(ProductDetailActivity.this.getResources().getDrawable(R.drawable.cicle_red), 300);
        animation.setOneShot(false);
        imageAnim.setBackgroundDrawable(animation);
        animation.start();

        AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        final String strProductId = AppsContants.sharedpreferences.getString(AppsContants.ProductId, "");
        String strProductName = AppsContants.sharedpreferences.getString(AppsContants.ProductName, "");
        String strImage = AppsContants.sharedpreferences.getString(AppsContants.Image, "");
        String strDiscount = AppsContants.sharedpreferences.getString(AppsContants.Dicount, "");
        String strSalePrice = AppsContants.sharedpreferences.getString(AppsContants.SalePrice, "");
        String strPrice = AppsContants.sharedpreferences.getString(AppsContants.Price, "");
        String strDescription = AppsContants.sharedpreferences.getString(AppsContants.Description, "");
        final String strUserId = AppsContants.sharedpreferences.getString(AppsContants.UserId, "");

        TextView txt_discription = findViewById(R.id.txt_discription);
        txt_discription.setText(strDescription);

        TextView txt_price = findViewById(R.id.txt_price);
        txt_price.setText(strPrice);

        TextView txt_dis_price = findViewById(R.id.txt_dis_price);
        txt_dis_price.setText(strSalePrice);
        TextView txt_product_name = findViewById(R.id.txt_product_name);
        txt_product_name.setText(strProductName);

        TextView txtDiscount = findViewById(R.id.txtDiscount);
        txtDiscount.setText(strDiscount + "%");

        imgProduct = findViewById(R.id.imgProduct);
        try {
            Glide.with(this)
                    .load(strImage)
                    .thumbnail(Glide.with(this).load(R.drawable.ezgifresize))
                    .into(imgProduct);
        } catch (Exception e) {
            e.printStackTrace();
        }


        Button btn_cart = findViewById(R.id.btn_cart);
        btn_cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AddToCart(strUserId, strProductId);
            }
        });

    }

    public void AddToCart(String strUserId, String strProductId) {

        final ProgressDialog progressDialog = new ProgressDialog(ProductDetailActivity.this);
        progressDialog.setMessage("Please wait..");
        progressDialog.setIndeterminate(true);
        progressDialog.show();
        progressDialog.setCancelable(false);

        AndroidNetworking.post(HTTPCALL.add_to_cart)
                .addBodyParameter("user_id", strUserId)
                .addBodyParameter("product_id", strProductId)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {

                            Log.e("response", response.toString());
                            if (response.getString("result").equals("Add cart successfully")) {
                                progressDialog.dismiss();
                                Toast.makeText(ProductDetailActivity.this, R.string.added_done, Toast.LENGTH_SHORT).show();

                            } else {
                                progressDialog.dismiss();
                                if(response.getString("result").equals("product already added in cart")){

                                    Toast.makeText(ProductDetailActivity.this, R.string.already_added, Toast.LENGTH_SHORT).show();
                                }

                            }

                        } catch (Exception ex) {
                            progressDialog.dismiss();
                            Log.e("Catch-Block", ex.toString());
                        }
                    }

                    @Override
                    public void onError(ANError anError) {

                        progressDialog.dismiss();
                        Log.e("onError", "Error" + anError.getMessage());
                    }
                });
    }
}