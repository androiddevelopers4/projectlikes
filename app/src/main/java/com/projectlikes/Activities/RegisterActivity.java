package com.projectlikes.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.projectlikes.R;
import com.projectlikes.Utils.HTTPCALL;

import org.json.JSONObject;

public class RegisterActivity extends AppCompatActivity {
    TextView txt_login;
    TextView txtAlreadyHaveAccount;
    EditText editFName;
    EditText LastName;
    EditText emailEmail;
    EditText ed_pass;
    EditText ed_confm_pass;
    EditText ed_ref;
    Button btn_signUp;
    Dialog dialog;
    String strFollowersStatus="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        txtAlreadyHaveAccount=findViewById(R.id.txtAlreadyHaveAccount);
        txtAlreadyHaveAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(RegisterActivity.this,LoginActivity.class));

            }
        });
        txt_login=findViewById(R.id.txt_login);
        txt_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(RegisterActivity.this,LoginActivity.class));

            }
        });


        editFName = findViewById(R.id.editFName);
        LastName = findViewById(R.id.LastName);
        emailEmail = findViewById(R.id.emailEmail);
        ed_pass = findViewById(R.id.ed_pass);
        ed_confm_pass = findViewById(R.id.ed_confm_pass);
        ed_ref = findViewById(R.id.ed_ref);
        btn_signUp = findViewById(R.id.btn_signUp);

        btn_signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final String strFName = editFName.getText().toString().trim();
                final String strLastName = LastName.getText().toString().trim();
                final String strEditEmail = emailEmail.getText().toString().trim();
                final String strPassword = ed_pass.getText().toString().trim();
                String strCPassword = ed_confm_pass.getText().toString().trim();
                final String strRefLink = ed_ref.getText().toString().trim();

                if (strFName.equals("")) {
                    Toast.makeText(RegisterActivity.this, R.string.enter_fname, Toast.LENGTH_SHORT).show();
                } else if (strLastName.equals("")) {
                    Toast.makeText(RegisterActivity.this, R.string.last_name, Toast.LENGTH_SHORT).show();
                } else if (strEditEmail.equals("")) {
                    Toast.makeText(RegisterActivity.this, R.string.enter_email, Toast.LENGTH_SHORT).show();
                } else if (strPassword.equals("")) {
                    Toast.makeText(RegisterActivity.this, R.string.enter_password, Toast.LENGTH_SHORT).show();
                } else if (!strCPassword.equals(strPassword)) {
                    Toast.makeText(RegisterActivity.this, R.string.password_not_match, Toast.LENGTH_SHORT).show();
                } else {


                    dialog = new Dialog(RegisterActivity.this);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setCancelable(false);
                    dialog.setContentView(R.layout.followers_popup);

                    final RadioButton radioFacebook = dialog.findViewById(R.id.radioFacebook);
                    final RadioButton radioInstagram = dialog.findViewById(R.id.radioInstagram);
                    final RadioButton radioYoutube = dialog.findViewById(R.id.radioYoutube);
                    final RadioButton radioTiktok = dialog.findViewById(R.id.radioTiktok);

                     radioFacebook.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                         @Override
                         public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                             if(b==true){

                                 strFollowersStatus="1";
                                 radioInstagram.setChecked(false);
                                 radioYoutube.setChecked(false);
                                 radioTiktok.setChecked(false);
                             }

                         }
                     });


                    radioInstagram.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                            if(b==true){

                                strFollowersStatus="2";
                                radioFacebook.setChecked(false);
                                radioYoutube.setChecked(false);
                                radioTiktok.setChecked(false);
                            }

                        }
                    });

                    radioTiktok.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                            if(b==true){

                                strFollowersStatus="3";
                                radioFacebook.setChecked(false);
                                radioInstagram.setChecked(false);
                                radioYoutube.setChecked(false);
                            }

                        }
                    });


                    radioYoutube.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                            if(b==true){

                                strFollowersStatus="4";
                                radioFacebook.setChecked(false);
                                radioInstagram.setChecked(false);
                                radioTiktok.setChecked(false);
                            }

                        }
                    });

                    Button dialog_ok = dialog.findViewById(R.id.dialog_ok);
                    dialog_ok.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if(strFollowersStatus.equals("")){

                                Toast.makeText(RegisterActivity.this, "Choose followers type", Toast.LENGTH_SHORT).show();
                            }
                            else {

                                dialog.dismiss();
                                Register(strFName,strLastName,strEditEmail,strPassword,strRefLink);
                            }

                        }
                    });

                    dialog.show();

                    //  startActivity(new Intent(Signup.this,VerifyOtpActivity.class));
                }


            }
        });
    }


    public void Register(String strFName, String strLastName, String strEditEmail, String strPassword, String strRefLink) {

      final ProgressDialog  progressDialog = new ProgressDialog(RegisterActivity.this);
        progressDialog.setMessage("Please wait..");
        progressDialog.setIndeterminate(true);
        progressDialog.show();
        progressDialog.setCancelable(false);

        AndroidNetworking.post(HTTPCALL.signup)
                .addBodyParameter("fname", strFName)
                .addBodyParameter("lname", strLastName)
                .addBodyParameter("email", strEditEmail)
                .addBodyParameter("password", strPassword)
                .addBodyParameter("gift_type", strFollowersStatus)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {

                            Log.e("response", response.toString());
                            if (response.getString("result").equals("Successfully Register")) {
                                progressDialog.dismiss();
                                Toast.makeText(RegisterActivity.this, R.string.reg_success, Toast.LENGTH_SHORT).show();
                                finish();
                            } else {
                                progressDialog.dismiss();
                                Toast.makeText(RegisterActivity.this, response.getString("result"), Toast.LENGTH_SHORT).show();
                            }

                        } catch (Exception ex) {
                            progressDialog.dismiss();
                            Log.e("Catch-Block", ex.toString());
                        }
                    }

                    @Override
                    public void onError(ANError anError) {

                        progressDialog.dismiss();
                        Log.e("onError", "Error" + anError.getMessage());
                    }
                });
    }
}