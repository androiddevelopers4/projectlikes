package com.projectlikes.Activities;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.projectlikes.MainActivity;
import com.projectlikes.R;
import com.projectlikes.Utils.AppsContants;
import com.projectlikes.Utils.LocaleUtils;

import java.util.List;

public class SplashActivity extends AppCompatActivity {

    String strLanguages="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        final String strUserId = AppsContants.sharedpreferences.getString(AppsContants.UserId, "");
        strLanguages = AppsContants.sharedpreferences.getString(AppsContants.language, "");
        Log.e("hasdjkad",strLanguages);

        if (strLanguages.equals("English")) {
            LocaleUtils.setLocale(SplashActivity.this, 0);
            SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
            editor.putString(AppsContants.language, "English");
            editor.commit();
        }
        else if(strLanguages.equals("Italic")) {

            LocaleUtils.setLocale(SplashActivity.this, 1);
            SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
            editor.putString(AppsContants.language, "Italic");
            editor.commit();
        }
        else {

            LocaleUtils.setLocale(SplashActivity.this, 0);
            SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
            editor.putString(AppsContants.language, "English");
            editor.commit();
        }


        Dexter.withActivity(this)
                .withPermissions(
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.CAMERA)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        // check if all permissions are granted
                        if (report.areAllPermissionsGranted()) {
                            new Handler().postDelayed(new Runnable() {

                                @Override
                                public void run() {

                                    if (strUserId.equals("")) {

                                        startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                                        finish();

                                    } else {

                                        startActivity(new Intent(SplashActivity.this, MainActivity.class));
                                        finish();
                                    }
                                }
                            }, 3000);
                        }

                        // check for permanent denial of any permission
                        if (report.isAnyPermissionPermanentlyDenied()) {
                            // permission is denied permenantly, navigate user to app settings
                            // Toast.makeText(SplashActivity.this, "Need Permission", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();

                    }
                })
                .check();



    }
}