package com.projectlikes.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.projectlikes.Adapters.ChatAdapter;
import com.projectlikes.Fragments.ChatModal;
import com.projectlikes.R;
import com.projectlikes.Utils.AppsContants;
import com.projectlikes.Utils.HTTPCALL;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class SupportChat extends AppCompatActivity {

    RecyclerView chatRecycler;
    List<ChatModal> chatModelList;
    EditText etSearch, etMsg;
    ImageView back;
    RelativeLayout rel_send;
    String strUserId, strMessage = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_support_chat);

        AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        strUserId = AppsContants.sharedpreferences.getString(AppsContants.UserId, "");

        ImageView back = findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        chatRecycler = findViewById(R.id.chatRecycler);
        back = findViewById(R.id.back);
        etMsg = findViewById(R.id.etMsg);
        rel_send = findViewById(R.id.rel_send);

        chatRecycler.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        chatRecycler.setHasFixedSize(true);
        chatRecycler.setItemViewCacheSize(20);


        rel_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                strMessage = etMsg.getText().toString().trim();
                if (strMessage.equals("")) {

                    Toast.makeText(SupportChat.this, "Enter Message", Toast.LENGTH_SHORT).show();
                } else {
                    sendMessages();
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        showChats();
    }

    public void showChats() {

        AndroidNetworking.post(HTTPCALL.show_message)
                .addBodyParameter("user_id", strUserId)
                .setTag("showChats")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @Override
                    public void onResponse(JSONArray response) {
                        chatModelList = new ArrayList<>();

                        try {
                            for (int i = 0; i < response.length(); i++) {
                                JSONObject object = response.getJSONObject(i);

                                ChatModal chatModel = new ChatModal();
                                chatModel.setId(object.getString("id"));
                                chatModel.setSender_id(object.getString("sender_id"));
                                chatModel.setReceiver_id(object.getString("recevier_id"));
                                chatModel.setMessage(object.getString("message"));
                                chatModel.setImage(object.getString("file"));
                                chatModel.setPath(object.getString("path"));
                                chatModelList.add(chatModel);
                            }

                            chatRecycler.setAdapter(new ChatAdapter(chatModelList, SupportChat.this));
                            if (chatModelList.size() != 0) {
                                chatRecycler.scrollToPosition(chatModelList.size() - 1);
                            }

                        } catch (Exception e) {
                            Log.e("euwyewd", e.getMessage(), e);
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.e("euwyewd", anError.getMessage(), anError);
                    }
                });
    }


    public void sendMessages() {

        AndroidNetworking.post(HTTPCALL.send_message)
                .addBodyParameter("sender_id", strUserId)
                .addBodyParameter("admin_id", "1")
                .addBodyParameter("msz", strMessage)
                .setTag("sendMsg")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                        Log.e("djsdcksl00d", response.toString());
                        try {

                            if (response.has("result")) {
                                String strResult = response.getString("result");
                                String message = response.getString("message");

                                if (strResult.equals("true")) {

                                    String data = response.getString("data");

                                    showChats();

                                } else {

                                    Toast.makeText(SupportChat.this, message, Toast.LENGTH_SHORT).show();

                                }
                            } else {

                                Toast.makeText(SupportChat.this, "Something went wrong", Toast.LENGTH_SHORT).show();

                            }

                        } catch (Exception e) {
                            Log.e("dsuwdiwdw", e.getMessage(), e);
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.e("dsuwdiwdw", anError.getMessage(), anError);
                    }
                });

    }
}