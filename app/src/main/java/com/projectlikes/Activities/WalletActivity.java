package com.projectlikes.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.projectlikes.Fragments.WalletAdapter;
import com.projectlikes.Modal.WalletModal;
import com.projectlikes.R;
import com.projectlikes.Utils.AppsContants;
import com.projectlikes.Utils.HTTPCALL;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class WalletActivity extends AppCompatActivity {

    TextView txtUserName;
    TextView txtCoins;
    RecyclerView recyclerView;
    ArrayList<WalletModal> walletModals;
    WalletAdapter walletAdapter;
    String strUserid = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_wallet);
        AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        strUserid = AppsContants.sharedpreferences.getString(AppsContants.UserId, "");
        String strFName = AppsContants.sharedpreferences.getString(AppsContants.fname, "");
        String strLName = AppsContants.sharedpreferences.getString(AppsContants.lname, "");


        txtUserName = findViewById(R.id.txtUserName);
        txtUserName.setText(strFName + " " + strLName);
        txtCoins = findViewById(R.id.txtCoins);
        ImageView imgBack = findViewById(R.id.imgBack);
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();
            }
        });

        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        GetHistory();

    }


    public void GetHistory() {

        final ProgressDialog progressDialogg = new ProgressDialog(WalletActivity.this);
        progressDialogg.setMessage("Please wait...");
        progressDialogg.setIndeterminate(true);
        progressDialogg.setCancelable(false);
        progressDialogg.show();

        AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        String strUserId = AppsContants.sharedpreferences.getString(AppsContants.UserId, "");
        AndroidNetworking.post(HTTPCALL.wallet_history)
                .addBodyParameter("user_id", strUserid)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {

                            Log.e("dfsdfsdsfsd", response.toString());
                            txtCoins.setText(response.getString("wallet_total_amount") + " €");
                            JSONArray jsonArray = new JSONArray(response.getString("wallet_history"));
                            walletModals = new ArrayList<>();

                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                WalletModal transactionModal = new WalletModal();
                                transactionModal.setId(jsonObject.getString("id"));
                                transactionModal.setPrice(jsonObject.getString("price"));
                                transactionModal.setName(jsonObject.getString("user_name"));
                                walletModals.add(transactionModal);
                            }
                            walletAdapter = new WalletAdapter(walletModals, WalletActivity.this);
                            recyclerView.setAdapter(walletAdapter);
                            progressDialogg.dismiss();

                        } catch (Exception ex) {
                            progressDialogg.dismiss();
                            Log.e("degkusdhgksdgf", ex.getMessage());
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        progressDialogg.dismiss();
                        Log.e("degkusdhgksdgf", "Error" + anError.getMessage());
                    }
                });

    }
}