package com.projectlikes.Adapters;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.projectlikes.Modal.AboutusModal;
import com.projectlikes.Modal.CartModal;
import com.projectlikes.R;

import java.util.ArrayList;

public class AboutUsAdapter extends RecyclerView.Adapter<AboutUsAdapter.ViewHolder> {
    Context context;
    ArrayList<AboutusModal>aboutArrayList;


    public AboutUsAdapter(Context context, ArrayList<AboutusModal>getDataAdapter){
        this.context=context;
        this.aboutArrayList=getDataAdapter;

    }

    @NonNull
    @Override
    public AboutUsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.rec_about_layout,parent,false);
        AboutUsAdapter.ViewHolder viewHolder=new AboutUsAdapter.ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull AboutUsAdapter.ViewHolder holder, int position) {

        AboutusModal aboutusModal=aboutArrayList.get(position);

        holder.txt_title.setText(aboutusModal.getTitle());
       // holder.txt_discription.setText(aboutusModal.getDiscription());
        holder.img_product.setImageResource(aboutusModal.getImg());

       holder.txt_discription.setText(Html.fromHtml(context.getString(R.string.sample_string)));

    }



    @Override
    public int getItemCount() {
        return  aboutArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView txt_title,txt_discription;
        ImageView img_product;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            txt_title=itemView.findViewById(R.id.txt_title);
            txt_discription=itemView.findViewById(R.id.txt_discription);
            img_product=itemView.findViewById(R.id.img_product);
        }
    }
}
