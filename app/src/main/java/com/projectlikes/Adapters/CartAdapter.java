package com.projectlikes.Adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.bumptech.glide.Glide;
import com.projectlikes.Fragments.CartFragment;
import com.projectlikes.Modal.CartModal;
import com.projectlikes.R;
import com.projectlikes.Utils.AppsContants;
import com.projectlikes.Utils.HTTPCALL;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class CartAdapter extends RecyclerView.Adapter<CartAdapter.ViewHolder> {
    Context context;
    ArrayList<CartModal>cartModalArrayList;
    CartFragment fragment;


    public CartAdapter (Context context, ArrayList<CartModal>getDataAdapter, CartFragment fragment){
        this.context=context;
        this.cartModalArrayList=getDataAdapter;
        this.fragment = fragment;

    }

    @NonNull
    @Override
    public CartAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.rec_cart_layout,parent,false);
        CartAdapter.ViewHolder viewHolder=new CartAdapter.ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull CartAdapter.ViewHolder holder, final int position) {

        final CartModal cartModal=cartModalArrayList.get(position);

        holder.txt_discount.setText(cartModal.getDiscount_percentage());
        holder.txt_price.setText(cartModal.getProduct_price());
        holder.txt_product_name.setText(cartModal.getProduct_name());
       // holder.img_product.setImageResource(cartModal.getImg());
        try {
            Glide.with(context)
                    .load(cartModal.getImg())
                    .thumbnail(Glide.with(context).load(R.drawable.ezgifresize))
                    .into(holder.img_product);
        } catch (Exception e) {
            e.printStackTrace();
        }


        holder.img_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                delete_cart(cartModal.getId(),position);
            }
        });


    }

    @Override
    public int getItemCount() {
        return  cartModalArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView txt_product_name,txt_price,txt_discount;
        ImageView img_product;
        ImageView img_delete;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            txt_discount=itemView.findViewById(R.id.txt_discount);
            txt_product_name=itemView.findViewById(R.id.txt_product_name);
            txt_price=itemView.findViewById(R.id.txt_price);
            img_product=itemView.findViewById(R.id.img_product);
            img_delete=itemView.findViewById(R.id.img_delete);
        }
    }


    public void delete_cart(String id, final int position) {
        AndroidNetworking.post(HTTPCALL.delete_to_cart)
                .addBodyParameter("cart_id",id)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("utyuyuu", response.toString());
                        try {
                            if (response.getString("result").equals("Successful  remove")) {
                                fragment.show_cart();
                                cartModalArrayList.remove(position);
                                notifyDataSetChanged();
                            }
                        } catch (JSONException e) {
                            Log.e("flkjdslkjf", e.getMessage());
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.e("rdegtrh", anError.getMessage());
                    }
                });


    }
}
