package com.projectlikes.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import com.projectlikes.Fragments.ChatModal;
import com.projectlikes.R;
import com.projectlikes.Utils.AppsContants;

import java.util.List;

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.ViewHolder> {

    Context context;
    List<ChatModal> dataAdapters;
    String strUserId="";

    public ChatAdapter(List<ChatModal> getDataAdapter, Context context) {

        super();
        this.dataAdapters = getDataAdapter;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_item_layout, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);

        AppsContants.sharedpreferences =context.getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        strUserId = AppsContants.sharedpreferences.getString(AppsContants.UserId, "");

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder Viewholder, final int position) {

        ChatModal dataAdapterOBJ = dataAdapters.get(position);

        if (dataAdapterOBJ.getSender_id().equals(strUserId)) {

            Viewholder.rl_sender.setVisibility(View.VISIBLE);
            Viewholder.rl_receiver.setVisibility(View.GONE);
            Viewholder.msgSender.setText(dataAdapterOBJ.getMessage());
        }
        else {

            Viewholder.rl_sender.setVisibility(View.VISIBLE);
            Viewholder.rl_receiver.setVisibility(View.GONE);
            Viewholder.msgReceiver.setText(dataAdapterOBJ.getMessage());
        }


    }

    @Override
    public int getItemCount() {

        return dataAdapters.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        RelativeLayout rl_sender,rl_receiver;
        ImageView imageSender,imageReceiver;
        TextView msgSender,msgReceiver;

        public ViewHolder(View itemView) {
            super(itemView);

            rl_sender = itemView.findViewById(R.id.rl_sender);
            imageSender = itemView.findViewById(R.id.imageSender);
            msgSender = itemView.findViewById(R.id.msgSender);

            rl_receiver = itemView.findViewById(R.id.rl_receiver);
            imageReceiver = itemView.findViewById(R.id.imageReceiver);
            msgReceiver = itemView.findViewById(R.id.msgReceiver);

        }
    }

}