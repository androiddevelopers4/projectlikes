package com.projectlikes.Adapters;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.kodmap.library.kmrecyclerviewstickyheader.KmStickyListener;
import com.makeramen.roundedimageview.RoundedImageView;
import com.projectlikes.Activities.ProductDetailActivity;
import com.projectlikes.Fragments.HomeFragment;
import com.projectlikes.Modal.FacebookModal;
import com.projectlikes.R;
import com.projectlikes.Utils.AppsContants;
import com.projectlikes.Utils.ItemType;
import com.squareup.picasso.Picasso;
import java.util.List;
public class FacebookAdapter extends RecyclerView.Adapter<FacebookAdapter.ViewHolder> {


    private Context context;
    List<FacebookModal> dataAdapters;

    public FacebookAdapter(List<FacebookModal> getDataAdapter,Context context) {
        this.context = context;
        this.dataAdapters = getDataAdapter;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.facebook_items, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {

        final FacebookModal dataAdapterOBJ = dataAdapters.get(position);
        holder.txtProdcutName.setText(dataAdapterOBJ.getName());
        holder.txtSalePrice.setText(dataAdapterOBJ.getSale_price());
        holder.txtPrice.setText(dataAdapterOBJ.getPrice());
        holder.txtDiscount.setText(dataAdapterOBJ.getDiscount());
        holder.txtFollowers.setText(dataAdapterOBJ.getFollow()+" "+"Followers");

        try {
            Glide.with(context)
                    .load(dataAdapterOBJ.getImage())
                    .thumbnail(Glide.with(context).load(R.drawable.ezgifresize))
                    .into(holder.imgLogo);
        } catch (Exception e) {
            e.printStackTrace();
        }



        holder.linearItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AppsContants.sharedpreferences = context.getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
                editor.putString(AppsContants.ProductId, dataAdapterOBJ.getItem_id());
                editor.putString(AppsContants.ProductName, dataAdapterOBJ.getName());
                editor.putString(AppsContants.ProductCatId,  dataAdapterOBJ.getCate_id());
                editor.putString(AppsContants.ProductCatName, dataAdapterOBJ.getCategory_name());
                editor.putString(AppsContants.Dicount,dataAdapterOBJ.getDiscount());
                editor.putString(AppsContants.Price, dataAdapterOBJ.getPrice());
                editor.putString(AppsContants.SalePrice, dataAdapterOBJ.getSale_price());
                editor.putString(AppsContants.Image, dataAdapterOBJ.getImage());
                editor.putString(AppsContants.Description, dataAdapterOBJ.getDesItem());
                editor.commit();

                context.startActivity(new Intent(context, ProductDetailActivity.class));
            }
        });

    }

    @Override
    public int getItemCount() {
        return dataAdapters.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView txtDiscount;
        TextView txtPrice;
        TextView txtSalePrice;
        TextView txtFollowers;
        TextView txtProdcutName;
        ImageView imgLogo;
        LinearLayout linearItem;
        CardView cardview1;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            cardview1 = itemView.findViewById(R.id.cardview1);
            imgLogo = itemView.findViewById(R.id.imgLogo);
            txtProdcutName = itemView.findViewById(R.id.txtProdcutName);
            txtFollowers = itemView.findViewById(R.id.txtFollowers);
            txtSalePrice = itemView.findViewById(R.id.txtSalePrice);
            txtPrice = itemView.findViewById(R.id.txtPrice);
            txtDiscount = itemView.findViewById(R.id.txtDiscount);
            linearItem = itemView.findViewById(R.id.linearItem);
        }
    }
}