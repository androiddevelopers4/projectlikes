package com.projectlikes.Adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.projectlikes.Modal.FacilityModal;
import com.projectlikes.R;
import com.projectlikes.Utils.AppsContants;

import java.util.List;

public class FacilityAdapter extends RecyclerView.Adapter<FacilityAdapter.ViewHolder> {

    Context context;
    FragmentManager fragmentManager;
    List<FacilityModal> dataAdapters;

    public FacilityAdapter(List<FacilityModal> getDataAdapter, Context context) {

        super();
        this.dataAdapters = getDataAdapter;
        this.context = context;
        this.fragmentManager = fragmentManager;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.facility_items, parent, false);

        ViewHolder viewHolder = new ViewHolder(view);



        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder Viewholder, final int position) {

        FacilityModal dataAdapterOBJ = dataAdapters.get(position);

        AppsContants.sharedpreferences =context.getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        String strLang = AppsContants.sharedpreferences.getString(AppsContants.language, "");

        if(strLang.equals("English")){

            Viewholder.txtTitle.setText(dataAdapterOBJ.getTitle());
            Viewholder.txtDescription.setText(dataAdapterOBJ.getDescription());
        }
        else {

            Viewholder.txtTitle.setText(dataAdapterOBJ.getIt_title());
            Viewholder.txtDescription.setText(dataAdapterOBJ.getIt_des());
        }



        try {
            Glide.with(context)
                    .load(dataAdapterOBJ.getIcon())
                    .thumbnail(Glide.with(context).load(R.drawable.ezgifresize))
                    .into(Viewholder.imgCategory);
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public int getItemCount() {

        return dataAdapters.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

      ImageView imgCategory;
      TextView txtTitle;
      TextView txtDescription;
        public ViewHolder(View itemView) {

            super(itemView);
            imgCategory=itemView.findViewById(R.id.imgCategory);
            txtTitle=itemView.findViewById(R.id.txtTitle);
            txtDescription=itemView.findViewById(R.id.txtDescription);
        }
    }

}