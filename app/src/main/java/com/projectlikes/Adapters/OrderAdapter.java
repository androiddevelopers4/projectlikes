package com.projectlikes.Adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.projectlikes.Modal.OrderModal;
import com.projectlikes.R;

import java.util.List;

public class OrderAdapter extends RecyclerView.Adapter<OrderAdapter.ViewHolder> {

    Context context;
    FragmentManager fragmentManager;
    List<OrderModal> dataAdapters;

    public OrderAdapter(List<OrderModal> getDataAdapter, Context context) {

        super();
        this.dataAdapters = getDataAdapter;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.order_item_adapter, parent, false);

        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder Viewholder, final int position) {

        OrderModal dataAdapterOBJ = dataAdapters.get(position);
        Viewholder.txtFolowers.setText(dataAdapterOBJ.getFollowers()+" "+"100");
        Viewholder.txtPrice.setText(dataAdapterOBJ.getPrice()+" "+"$");
        Viewholder.txtCategory.setText(dataAdapterOBJ.getLink());
        Viewholder.txtOrderId.setText(dataAdapterOBJ.getOrderId());
        Viewholder.txtDate.setText(dataAdapterOBJ.getDate());
        Log.e("adasdad","image"+dataAdapterOBJ.getImage());
        try {
            Glide.with(context)
                    .load(dataAdapterOBJ.getImage())
                    .thumbnail(Glide.with(context).load(R.drawable.ezgifresize))
                    .into(Viewholder.imgProduct);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {

        return dataAdapters.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        ImageView imgProduct;
        TextView txtDate;
        TextView txtCategory;
        TextView txtPrice;
        TextView txtFolowers;
        TextView txtOrderId;


        public ViewHolder(View itemView) {

            super(itemView);

            txtOrderId=itemView.findViewById(R.id.txtOrderId);
            txtFolowers=itemView.findViewById(R.id.txtFolowers);
            txtPrice=itemView.findViewById(R.id.txtPrice);
            txtCategory=itemView.findViewById(R.id.txtCategory);
            txtDate=itemView.findViewById(R.id.txtDate);
            imgProduct=itemView.findViewById(R.id.imgProduct);

        }
    }

}