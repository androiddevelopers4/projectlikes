package com.projectlikes.Adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.projectlikes.Modal.FacebookModal;
import com.projectlikes.R;
import com.projectlikes.Utils.AppsContants;
import com.projectlikes.Utils.HTTPCALL;
import com.projectlikes.Utils.ItemType;

import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

public class ProductHeaderAdapter extends RecyclerView.Adapter<ProductHeaderAdapter.ViewHolder> {

    Context context;
    List<FacebookModal> dataAdapters;

    FacebookAdapter facebookAdapter;
    List<FacebookModal> facebookModals;
    LinearLayoutManager linearLayoutManager;

    public ProductHeaderAdapter(List<FacebookModal> getDataAdapter, Context context) {

        super();
        this.dataAdapters = getDataAdapter;
        this.context = context;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_header, parent, false);

        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder Viewholder, final int position) {

        FacebookModal dataAdapterOBJ = dataAdapters.get(position);


        AppsContants.sharedpreferences =context.getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        String strLang = AppsContants.sharedpreferences.getString(AppsContants.language, "");

        if(strLang.equals("English")){

            Viewholder.txtHeaderTitle.setText(dataAdapterOBJ.getCategory());
            Viewholder.txtHeaderDesc.setText(dataAdapterOBJ.getDes());
        }
        else {

            Viewholder.txtHeaderTitle.setText(dataAdapterOBJ.getIt_category());
            Viewholder.txtHeaderDesc.setText(dataAdapterOBJ.getIt_des());
        }



        ShowFeaturedItems(dataAdapterOBJ.getId(),Viewholder);

    }

    @Override
    public int getItemCount() {

        return dataAdapters.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

         RecyclerView recyclerviewFacebook;
         TextView txtHeaderDesc;
         TextView txtHeaderTitle;
        public ViewHolder(View itemView) {

            super(itemView);
            recyclerviewFacebook = itemView.findViewById(R.id.recyclerviewFacebook);
            txtHeaderDesc = itemView.findViewById(R.id.txtHeaderDesc);
            txtHeaderTitle = itemView.findViewById(R.id.txtHeaderTitle);

        }
    }




    public void ShowFeaturedItems(final String id, final ViewHolder viewholder) {

        AndroidNetworking.get(HTTPCALL.show_product)
                .setTag("Show products")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.e("uifoufv", response.toString());
                        facebookModals = new ArrayList<>();
                        try {
                            for (Integer i = 0; i < response.length(); i++) {
                                JSONObject jsonObject = response.getJSONObject(i);

                                JSONArray jsonArray = new JSONArray(jsonObject.getString("product"));
                                for (Integer j = 0; j < jsonArray.length(); j++) {
                                    JSONObject jsonObject2 = jsonArray.getJSONObject(j);
                                    if(id.equals(jsonObject2.getString("cate_id"))) {
                                        FacebookModal featuredPost = new FacebookModal();
                                        featuredPost.setItem_id(jsonObject2.getString("id"));
                                        featuredPost.setCate_id(jsonObject2.getString("cate_id"));
                                        featuredPost.setName(jsonObject2.getString("name"));
                                        featuredPost.setIt_name(jsonObject2.getString("it_name"));
                                        featuredPost.setFollow(jsonObject2.getString("follow"));
                                        featuredPost.setPrice(jsonObject2.getString("price"));
                                        featuredPost.setDiscount(jsonObject2.getString("discount"));
                                        featuredPost.setSale_price(jsonObject2.getString("sale_price"));
                                        featuredPost.setDesItem(jsonObject2.getString("des"));
                                        featuredPost.setIt_des_item(jsonObject2.getString("it_des"));
                                        featuredPost.setCategory_name(jsonObject2.getString("category_name"));
                                        featuredPost.setImage(jsonObject2.getString("path") + jsonObject2.getString("image"));
                                        facebookModals.add(featuredPost);
                                    }

                                    facebookAdapter = new FacebookAdapter(facebookModals,context);
                                    linearLayoutManager =new LinearLayoutManager(context,LinearLayoutManager.HORIZONTAL,true);
                                    viewholder.recyclerviewFacebook.setLayoutManager(linearLayoutManager);
                                    viewholder.recyclerviewFacebook.setAdapter(facebookAdapter);

                                }
                            }



                        } catch (Exception e) {
                            Log.e("efrdegf", e.getMessage());

                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.e("etfgr", anError.getMessage());

                    }
                });

    }

}