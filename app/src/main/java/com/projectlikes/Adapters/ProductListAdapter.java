package com.projectlikes.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.projectlikes.Activities.ProductDetailActivity;
import com.projectlikes.Modal.CartModal;
import com.projectlikes.Modal.ProductListItemModal;
import com.projectlikes.Modal.ProductListModal;
import com.projectlikes.R;

import java.util.ArrayList;

public class ProductListAdapter extends RecyclerView.Adapter<ProductListAdapter.ViewHolder> {
    Context context;
    ArrayList<ProductListModal>listModals;
    public ProductListAdapter(Context context, ArrayList<ProductListModal>getDataAdapter){
        this.context=context;
        this.listModals=getDataAdapter;

    }

    @NonNull
    @Override
    public ProductListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.rec_productlist_layout,parent,false);
        ProductListAdapter.ViewHolder viewHolder=new ProductListAdapter.ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ProductListAdapter.ViewHolder holder, int position) {

        final ProductListModal productListModal=listModals.get(position);


        holder.txt_product_title.setText(productListModal.getProduct_title());
        holder.txt_discrptn.setText(productListModal.getDiscription());


    //    ArrayList<ProductListItemModal> singleSectionItems = productListModal.getArrayList();

       // holder.tvTitle.setText(productListModal.getCategory());

   /*     ProductListItemAdapter itemListDataAdapter =
                new ProductListItemAdapter(context, singleSectionItems);
*/
        holder.rec_item.setHasFixedSize(true);
        holder.rec_item.setLayoutManager(new LinearLayoutManager(context,
                LinearLayoutManager.HORIZONTAL, false));
      //  holder.rec_item.setAdapter(itemListDataAdapter);

        holder.rec_item.setNestedScrollingEnabled(false);

        /*holder.btnMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, productListModal.getCategory(), Toast.LENGTH_SHORT).show();
            }
        });*/

    }

    @Override
    public int getItemCount() {
        return  listModals.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView txt_discrptn,txt_product_title;
      RecyclerView rec_item;



        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            txt_discrptn=itemView.findViewById(R.id.txt_discrptn);
            rec_item=itemView.findViewById(R.id.rec_item);
            txt_product_title=itemView.findViewById(R.id.txt_product_title);
        }
    }
}
