package com.projectlikes.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.projectlikes.Activities.ProductDetailActivity;
import com.projectlikes.Modal.ProductListItemModal;
import com.projectlikes.Modal.ProductListModal;
import com.projectlikes.R;

import java.util.ArrayList;

public class ProductListItemAdapter extends RecyclerView.Adapter<ProductListItemAdapter.ViewHolder> {
    Context context;
    ArrayList<ProductListItemModal>listModalsitem;


    public ProductListItemAdapter(Context context, ArrayList<ProductListItemModal>getDataAdapter){
        this.context=context;
        this.listModalsitem=getDataAdapter;

    }

    @NonNull
    @Override
    public ProductListItemAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.rec_show_item_layout,parent,false);
        ProductListItemAdapter.ViewHolder viewHolder=new ProductListItemAdapter.ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ProductListItemAdapter.ViewHolder holder, int position) {

        ProductListItemModal productListModal=listModalsitem.get(position);

        holder.txt_price.setText(productListModal.getPrice());
        holder.txt_dis_price.setText(productListModal.getFinal_price());
        holder.txt_followersNo.setText(productListModal.getFollowers_no());
        holder.txt_product_name.setText(productListModal.getProduct_name());
        holder.txt_percent.setText(productListModal.getPercent());

        holder.img_product.setImageResource(productListModal.getImage());

        holder.rl_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                context.startActivity(new Intent(context, ProductDetailActivity.class));
            }
        });

    }

    @Override
    public int getItemCount() {
        return  listModalsitem.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView txt_product_name,txt_followersNo,txt_percent,txt_dis_price,txt_discrptn,txt_price,txt_product_title;
        ImageView img_product;
        RelativeLayout rl_list;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            txt_followersNo=itemView.findViewById(R.id.txt_followersNo);
            txt_product_name=itemView.findViewById(R.id.txt_product_name);
            txt_percent=itemView.findViewById(R.id.txt_percent);
            txt_discrptn=itemView.findViewById(R.id.txt_discrptn);
            img_product=itemView.findViewById(R.id.img_product);
            rl_list=itemView.findViewById(R.id.rl_list);
            txt_dis_price=itemView.findViewById(R.id.txt_dis_price);
            txt_price=itemView.findViewById(R.id.txt_price);
            txt_product_title=itemView.findViewById(R.id.txt_product_title);
        }
    }
}
