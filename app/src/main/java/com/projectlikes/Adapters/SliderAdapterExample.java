package com.projectlikes.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.makeramen.roundedimageview.RoundedImageView;
import com.projectlikes.Modal.SliderModel;
import com.projectlikes.R;
import com.smarteist.autoimageslider.SliderViewAdapter;
import com.squareup.picasso.Picasso;

import java.util.List;

public class SliderAdapterExample extends SliderViewAdapter<SliderAdapterExample.SliderAdapterVH> {

    private Context context;
    List<SliderModel> dataAdapters;

    public SliderAdapterExample(List<SliderModel> getDataAdapter, Context context) {
        this.context = context;
        this.dataAdapters = getDataAdapter;
    }

    @Override
    public SliderAdapterVH onCreateViewHolder(ViewGroup parent) {
        View inflate = LayoutInflater.from(parent.getContext()).inflate(R.layout.image_slider_layout_item, null);
        return new SliderAdapterVH(inflate);
    }

    @Override
    public void onBindViewHolder(SliderAdapterVH viewHolder, final int position) {
        final SliderModel dataAdapterOBJ = dataAdapters.get(position);

      //  viewHolder.textViewDescription.setText(dataAdapterOBJ.getTitle());
        try {
            Glide.with(context)
                    .load(dataAdapterOBJ.getImage())
                    .thumbnail(Glide.with(context).load(R.drawable.ezgifresize))
                    .into(viewHolder.imgSlider);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getCount() {
        //slider view count could be dynamic size
        return dataAdapters.size();
    }

    class SliderAdapterVH extends SliderViewAdapter.ViewHolder {

        View itemView;
        ImageView imgSlider;
        TextView textViewDescription;

        public SliderAdapterVH(View itemView) {
            super(itemView);
            imgSlider = itemView.findViewById(R.id.imgSlider);
            // textViewDescription = itemView.findViewById(R.id.txt_details);
            this.itemView = itemView;
        }
    }


}