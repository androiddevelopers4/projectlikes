package com.projectlikes.Fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Paint;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.paypal.android.sdk.payments.PayPalService;
import com.projectlikes.Activities.ContinuePaypal;
import com.projectlikes.Activities.ProductDetailActivity;
import com.projectlikes.Adapters.CartAdapter;
import com.projectlikes.Adapters.ProductHeaderAdapter;
import com.projectlikes.Modal.CartModal;
import com.projectlikes.Modal.FacebookModal;
import com.projectlikes.R;
import com.projectlikes.Utils.AppsContants;
import com.projectlikes.Utils.HTTPCALL;
import com.projectlikes.Utils.ItemType;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Random;


public class CartFragment extends Fragment {

    RecyclerView.LayoutManager layoutManager;
    CartAdapter cartAdapter;
    ArrayList<CartModal>arrayListCart=new ArrayList<>();
    RecyclerView rec_cart;
    String strUserId;
    TextView txt_total_item;
    TextView txt_total_price_new;
    TextView txt_final_price;

    Button btnAddLink;
    LinearLayout linearEdit;
    ArrayList<EditText>arrayListEdit;
    EditText editText;
    String strCartId="";
    int number=0;
    String strLinks="";
    String total_price="";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_cart, container, false);

        AppsContants.sharedpreferences = getActivity().getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        strUserId = AppsContants.sharedpreferences.getString(AppsContants.UserId, "");
        Log.e("sdfsdfbsdfs",strUserId);
        txt_total_item=view.findViewById(R.id.txt_total_item);
        txt_total_price_new=view.findViewById(R.id.txt_total_price_new);
        txt_final_price=view.findViewById(R.id.txt_final_price);

        rec_cart=view.findViewById(R.id.rec_cart);
        rec_cart.setHasFixedSize(true);
        layoutManager=new LinearLayoutManager(getActivity(),RecyclerView.VERTICAL,false);
        rec_cart.setLayoutManager(layoutManager);
        arrayListEdit= new ArrayList<>();


        Button btn_checkout=view.findViewById(R.id.btn_checkout);
        btn_checkout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                for (int i = 0; i <arrayListEdit.size() ; i++) {
                    if(strLinks.equals("")){
                        strLinks=arrayListEdit.get(i).getText().toString();
                    }
                    else {
                        strLinks=strLinks+","+arrayListEdit.get(i).getText().toString();
                    }
                }


                AppsContants.sharedpreferences = getActivity().getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
                editor.putString(AppsContants.CartId, strCartId);
                editor.putString(AppsContants.Links, strLinks);
                editor.putString(AppsContants.TotalPrice, total_price);
                editor.commit();


                Log.e("sdfsdfbsdfs",strCartId);
                Log.e("sdfsdfbsdfs",strLinks);
                startActivity(new Intent(getActivity(), ContinuePaypal.class));
            }
        });
        show_cart();

        btnAddLink = view.findViewById(R.id.btnAddLink);
        linearEdit = view.findViewById(R.id.linearEdit);

        Random rnd = new Random();
        number = rnd.nextInt(999999);

        editText = new EditText(getActivity());
        editText.setId(number);
        editText.setHint(R.string.enter_link);
        editText.setBackground(getResources().getDrawable(R.drawable.rel_paypal));
        linearEdit.addView(editText);
        editText.setTextSize(14);
        editText.setHeight(80);
        editText.setPadding(10, 0, 10, 0);
        setMargins(editText, 15, 10, 15, 10);
        arrayListEdit.add(editText);

        btnAddLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Random rnd = new Random();
                number = rnd.nextInt(999999);

                editText = new EditText(getActivity());
                editText.setId(number);
                editText.setHint(R.string.enter_link);
                editText.setBackground(getResources().getDrawable(R.drawable.rel_paypal));
                linearEdit.addView(editText);


                editText.setTextSize(14);
                editText.setHeight(80);
                editText.setPadding(10, 0, 10, 0);
                setMargins(editText, 15, 10, 15, 10);
                arrayListEdit.add(editText);
            }
        });

        return view;
    }

    private void setMargins(View view, int left, int top, int right, int bottom) {
        if (view.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
            ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
            p.setMargins(left, top, right, bottom);
            view.requestLayout();
        }
    }

    public void show_cart(){

        final ProgressDialog progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Please wait..");
        progressDialog.setIndeterminate(true);
        progressDialog.show();
        progressDialog.setCancelable(false);


        AndroidNetworking.post(HTTPCALL.show_cart)
                .addBodyParameter("user_id",strUserId)
                .setTag("Show products")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("uifoufv", response.toString());
                        arrayListCart = new ArrayList<>();
                        try {

                             total_price=response.getString("total_price");
                             strCartId=response.getString("unique_cart_id");

                             txt_final_price.setText(total_price);
                             txt_total_price_new.setText(total_price);

                             String no_of_items=response.getString("no_of_items");
                             txt_total_item.setText(no_of_items);


                             JSONArray jsonArray=new JSONArray(response.getString("data"));
                             for (Integer i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                CartModal modal=new CartModal( jsonObject.getString("cart_id"),jsonObject.getString("path")+jsonObject.getString("image"),
                                        jsonObject.getString("name"),
                                        jsonObject.getString("discount"),
                                        jsonObject.getString("price"));
                                arrayListCart.add(modal);
                            }

                            cartAdapter=new CartAdapter(getActivity(),arrayListCart,CartFragment.this);
                            rec_cart.setAdapter(cartAdapter);
                            progressDialog.dismiss();

                        }
                        catch (Exception e) {
                            progressDialog.dismiss();
                            Log.e("efrdegf", e.getMessage());

                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        progressDialog.dismiss();
                        Log.e("etfgr", anError.getMessage());

                    }
                });

    }
}