package com.projectlikes.Fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSnapHelper;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.kodmap.library.kmrecyclerviewstickyheader.KmHeaderItemDecoration;
import com.projectlikes.Activities.SplashActivity;
import com.projectlikes.Adapters.FacebookAdapter;
import com.projectlikes.Adapters.FacilityAdapter;
import com.projectlikes.Adapters.ProductHeaderAdapter;
import com.projectlikes.Adapters.SliderAdapterExample;
import com.projectlikes.Modal.FacebookModal;
import com.projectlikes.Modal.FacilityModal;
import com.projectlikes.Modal.SliderModel;
import com.projectlikes.R;
import com.projectlikes.Utils.AppsContants;
import com.projectlikes.Utils.HTTPCALL;
import com.projectlikes.Utils.ItemType;
import com.projectlikes.Utils.LocaleUtils;
import com.smarteist.autoimageslider.IndicatorAnimations;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class HomeFragment extends Fragment {

    SliderAdapterExample sliderAdapter;
    List<SliderModel> listOfSlider;
    SliderView sliderView;

    RecyclerView recyclerviewFacebook;
    ProductHeaderAdapter productHeaderAdapter;
    List<FacebookModal> facebookModals;
    LinearLayoutManager linearLayoutManager;

    FacilityAdapter facilityAdapter;
    List<FacilityModal> listOfFacility;
    RecyclerView recyclerFacility;

    public static Context context;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        context = getActivity();
        sliderView = view.findViewById(R.id.imageSlider);
        recyclerviewFacebook = view.findViewById(R.id.recyclerviewFacebook);
        recyclerviewFacebook.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerviewFacebook.setHasFixedSize(true);


        recyclerFacility = view.findViewById(R.id.recyclerFacility);
        recyclerFacility.setHasFixedSize(true);
        linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, true);
        recyclerFacility.setLayoutManager(linearLayoutManager);
        LinearSnapHelper linearSnapHelpers = new LinearSnapHelper();
        linearSnapHelpers.attachToRecyclerView(recyclerFacility);


        ShowSliderImages();
        ShowFeaturedItems();
        ShowFacility();


        ImageView imageView = view.findViewById(R.id.imgLaguage);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                alert.setMessage(R.string.change_laguage)
                        .setPositiveButton(R.string.english, new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int which) {

                                LocaleUtils.setLocale(getActivity(), 0);
                                AppsContants.sharedpreferences = getActivity().getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
                                editor.putString(AppsContants.language, "English");
                                editor.commit();
                                startActivity(new Intent(getActivity(), SplashActivity.class));
                                getActivity().finish();

                            }
                        }).setNegativeButton(R.string.italian, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        LocaleUtils.setLocale(getActivity(), 1);
                        AppsContants.sharedpreferences = getActivity().getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
                        editor.putString(AppsContants.language, "Italic");
                        editor.commit();

                        startActivity(new Intent(getActivity(), SplashActivity.class));
                        getActivity().finish();
                    }
                });

                AlertDialog alert1 = alert.create();
                alert1.show();
            }
        });

        return view;
    }


    public void ShowFeaturedItems() {

        AndroidNetworking.get(HTTPCALL.show_product)
                .setTag("Show products")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.e("uifoufv", response.toString());
                        facebookModals = new ArrayList<>();
                        try {
                            for (Integer i = 0; i < response.length(); i++) {
                                JSONObject jsonObject = response.getJSONObject(i);

                                FacebookModal featuredHeader = new FacebookModal();
                                featuredHeader.setType(ItemType.Header);
                                featuredHeader.setId(jsonObject.getString("id"));
                                featuredHeader.setCategory(jsonObject.getString("category"));
                                featuredHeader.setIt_category(jsonObject.getString("it_category"));
                                featuredHeader.setDes(jsonObject.getString("des"));
                                featuredHeader.setIt_des(jsonObject.getString("it_des"));
                                facebookModals.add(featuredHeader);

                            }

                            productHeaderAdapter = new ProductHeaderAdapter(facebookModals, getActivity());
                            recyclerviewFacebook.setAdapter(productHeaderAdapter);


                        } catch (Exception e) {
                            Log.e("efrdegf", e.getMessage());

                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.e("etfgr", anError.getMessage());

                    }
                });

    }

    public void ShowSliderImages() {
        sliderView.setIndicatorAnimation(IndicatorAnimations.WORM); //set indicator animation by using SliderLayout.IndicatorAnimations. :WORM or THIN_WORM or COLOR or DROP or FILL or NONE or SCALE or SCALE_DOWN or SLIDE and SWAP!!
        sliderView.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION);
        sliderView.setAutoCycleDirection(SliderView.AUTO_CYCLE_DIRECTION_BACK_AND_FORTH);
        sliderView.setIndicatorSelectedColor(Color.WHITE);
        sliderView.setIndicatorUnselectedColor(Color.GRAY);
        sliderView.setScrollTimeInSec(3); //set scroll delay in seconds :
        sliderView.startAutoCycle();
        AndroidNetworking.get(HTTPCALL.main_banner)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.e("sdfsdfsfsfs", response.toString());
                        listOfSlider = new ArrayList<>();
                        try {
                            for (int i = 0; i < response.length(); i++) {
                                JSONObject jsonObject = response.getJSONObject(i);
                                SliderModel sliderModel = new SliderModel();
                                sliderModel.setId(jsonObject.getString("id"));
                                sliderModel.setTitle(jsonObject.getString("title"));
                                sliderModel.setImage(jsonObject.getString("path") + jsonObject.getString("image"));
                                listOfSlider.add(sliderModel);
                            }

                            sliderAdapter = new SliderAdapterExample(listOfSlider, getActivity());
                            sliderView.setSliderAdapter(sliderAdapter);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });


    }


    public void ShowFacility() {

        AndroidNetworking.get(HTTPCALL.facility)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.e("sdfsdfsfsfs", response.toString());
                        listOfFacility = new ArrayList<>();
                        try {
                            for (int i = 0; i < response.length(); i++) {
                                JSONObject jsonObject = response.getJSONObject(i);
                                FacilityModal sliderModel = new FacilityModal();
                                sliderModel.setId(jsonObject.getString("id"));
                                sliderModel.setTitle(jsonObject.getString("title"));
                                sliderModel.setDescription(jsonObject.getString("des"));
                                sliderModel.setIt_title(jsonObject.getString("it_title"));
                                sliderModel.setIt_des(jsonObject.getString("it_des"));
                                sliderModel.setIcon(jsonObject.getString("path") + jsonObject.getString("icon"));
                                listOfFacility.add(sliderModel);
                            }

                            facilityAdapter = new FacilityAdapter(listOfFacility, getActivity());
                            recyclerFacility.setAdapter(facilityAdapter);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });


    }

}