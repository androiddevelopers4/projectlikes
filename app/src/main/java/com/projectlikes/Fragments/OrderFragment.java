package com.projectlikes.Fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.projectlikes.Adapters.FacilityAdapter;
import com.projectlikes.Adapters.OrderAdapter;
import com.projectlikes.Modal.FacilityModal;
import com.projectlikes.Modal.OrderModal;
import com.projectlikes.R;
import com.projectlikes.Utils.AppsContants;
import com.projectlikes.Utils.HTTPCALL;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class OrderFragment extends Fragment {


    RecyclerView recyclerview;
    LinearLayoutManager linearLayoutManager;
    OrderAdapter orderAdapter;
    List<OrderModal> orderModalList;
    String strUserId="";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_order, container, false);

        AppsContants.sharedpreferences = getActivity().getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        strUserId = AppsContants.sharedpreferences.getString(AppsContants.UserId, "");
        Log.e("erterte",strUserId);

        AppsContants.sharedpreferences = getActivity().getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
        editor.putString(AppsContants.OrderFragmentStatus, "");
        editor.commit();


        recyclerview=view.findViewById(R.id.recyclerview);
        recyclerview.setHasFixedSize(true);

        linearLayoutManager=new LinearLayoutManager(getActivity());
        recyclerview.setLayoutManager(linearLayoutManager);

        return view;
    }


    @Override
    public void onResume() {
        super.onResume();
        ShowOrder();
    }

    public void ShowOrder() {

        final ProgressDialog progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage(getString(R.string.wait));
        progressDialog.setIndeterminate(true);
        progressDialog.show();
        progressDialog.setCancelable(false);

        AndroidNetworking.post(HTTPCALL.orders)
                .addBodyParameter("user_id",strUserId)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.e("sdfsdfsfsfs", response.toString());
                        orderModalList = new ArrayList<>();
                        try {
                            for (int i = 0; i < response.length(); i++) {

                                JSONObject jsonObject = response.getJSONObject(i);
                                OrderModal orderModal = new OrderModal();
                                orderModal.setOrderId(jsonObject.getString("order_id"));
                                orderModal.setId(jsonObject.getString("id"));
                                orderModal.setPrice(jsonObject.getString("total_price"));
                                orderModal.setFollowers(jsonObject.getString("follower"));
                                orderModal.setDate(jsonObject.getString("order_date"));
                                orderModal.setName(jsonObject.getString("product_name"));
                                orderModal.setLink(jsonObject.getString("link"));
                                orderModal.setImage(jsonObject.getString("path")+jsonObject.getString("product_image"));
                                orderModalList.add(orderModal);
                            }

                            orderAdapter = new OrderAdapter(orderModalList, getActivity());
                            recyclerview.setAdapter(orderAdapter);
                            progressDialog.dismiss();

                        } catch (JSONException e) {

                            progressDialog.dismiss();

                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        progressDialog.dismiss();
                    }
                });


    }

}