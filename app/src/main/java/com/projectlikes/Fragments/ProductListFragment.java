package com.projectlikes.Fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSnapHelper;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.projectlikes.Activities.ProductDetailActivity;
import com.projectlikes.Adapters.CartAdapter;
import com.projectlikes.Adapters.FacilityAdapter;
import com.projectlikes.Adapters.ProductHeaderAdapter;
import com.projectlikes.Adapters.ProductListAdapter;
import com.projectlikes.Adapters.SliderAdapterExample;
import com.projectlikes.Modal.CartModal;
import com.projectlikes.Modal.FacebookModal;
import com.projectlikes.Modal.FacilityModal;
import com.projectlikes.Modal.ProductListModal;
import com.projectlikes.Modal.SliderModel;
import com.projectlikes.R;
import com.projectlikes.Utils.HTTPCALL;
import com.projectlikes.Utils.ItemType;
import com.smarteist.autoimageslider.IndicatorAnimations;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class ProductListFragment extends Fragment {



    RecyclerView recyclerviewFacebook;
    ProductHeaderAdapter productHeaderAdapter;
    List<FacebookModal> facebookModals;
    LinearLayoutManager linearLayoutManager;


    public static Context context;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_product_list, container, false);


        context = getActivity();
        recyclerviewFacebook = view.findViewById(R.id.recyclerviewFacebook);
        recyclerviewFacebook.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerviewFacebook.setHasFixedSize(true);

        ShowFeaturedItems();

        return view;
    }
    public void ShowFeaturedItems() {

        AndroidNetworking.get(HTTPCALL.show_product)
                .setTag("Show products")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.e("uifoufv", response.toString());
                        facebookModals = new ArrayList<>();
                        try {
                            for (Integer i = 0; i < response.length(); i++) {
                                JSONObject jsonObject = response.getJSONObject(i);

                                FacebookModal featuredHeader = new FacebookModal();
                                featuredHeader.setType(ItemType.Header);
                                featuredHeader.setId(jsonObject.getString("id"));
                                featuredHeader.setCategory(jsonObject.getString("category"));
                                featuredHeader.setIt_category(jsonObject.getString("it_category"));
                                featuredHeader.setDes(jsonObject.getString("des"));
                                featuredHeader.setIt_des(jsonObject.getString("it_des"));
                                facebookModals.add(featuredHeader);

                            }

                            productHeaderAdapter = new ProductHeaderAdapter(facebookModals, getActivity());
                            recyclerviewFacebook.setAdapter(productHeaderAdapter);



                        } catch (Exception e) {
                            Log.e("efrdegf", e.getMessage());

                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.e("etfgr", anError.getMessage());

                    }
                });

    }


}