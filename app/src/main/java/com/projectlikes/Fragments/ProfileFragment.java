package com.projectlikes.Fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.projectlikes.Activities.AboutUsActivity;
import com.projectlikes.Activities.ContactUsActivity;
import com.projectlikes.Activities.LoginActivity;
import com.projectlikes.Activities.MyProfileActivity;
import com.projectlikes.Activities.RegisterActivity;
import com.projectlikes.Activities.SplashActivity;
import com.projectlikes.Activities.SupportChat;
import com.projectlikes.Activities.WalletActivity;
import com.projectlikes.R;
import com.projectlikes.Utils.AppsContants;


public class ProfileFragment extends Fragment {

RelativeLayout rl_profile,rl_about,rl_contact,rl_logout,rl_wallet,rl_share,rl_Chat;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_profile, container, false);

        rl_share=view.findViewById(R.id.rl_share);
        rl_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT,getString(R.string.share_link));
                sendIntent.setType("text/plain");
                startActivity(sendIntent);

            }
        });


        rl_profile=view.findViewById(R.id.rl_profile);
        rl_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), MyProfileActivity.class));

            }
        });

        rl_wallet=view.findViewById(R.id.rl_wallet);
        rl_wallet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), WalletActivity.class));

            }
        });

        rl_about=view.findViewById(R.id.rl_about);
        rl_about.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), AboutUsActivity.class));
            }
        });
        rl_Chat=view.findViewById(R.id.rl_Chat);
        rl_Chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), SupportChat.class));
            }
        });



        rl_logout=view.findViewById(R.id.rl_logout);
        rl_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPopup();
            }
        });
        rl_contact=view.findViewById(R.id.rl_contact);
        rl_contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), ContactUsActivity.class));

            }
        });

        return view;
    }



    private void showPopup() {
        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
        alert.setMessage(R.string.are_you_sure)
                .setPositiveButton(R.string.logout, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {

                        logout(); // Last step. Logout function

                    }
                }).setNegativeButton(R.string.cancel, null);

        AlertDialog alert1 = alert.create();
        alert1.show();
    }

    private void logout() {


        AppsContants.sharedpreferences = getActivity().getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
        editor.putString(AppsContants.UserId, "");
        editor.commit();
        startActivity(new Intent(getActivity(), SplashActivity.class));

    }
}