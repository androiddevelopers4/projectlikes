package com.projectlikes.Fragments;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import com.projectlikes.Modal.WalletModal;
import com.projectlikes.R;

import java.util.List;

public class WalletAdapter extends RecyclerView.Adapter<WalletAdapter.ViewHolder> {

    Context context;
    List<WalletModal> dataAdapters;


    public WalletAdapter(List<WalletModal> getDataAdapter, Context context) {
        super();
        this.dataAdapters = getDataAdapter;
        this.context = context;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.wallet_items, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder Viewholder, final int position) {

        WalletModal dataAdapterOBJ = dataAdapters.get(position);
        Viewholder.tv_price.setText(dataAdapterOBJ.getPrice()+" €");
        Viewholder.txtStatus.setText(dataAdapterOBJ.getName());
    }

    @Override
    public int getItemCount() {
        return dataAdapters.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView txtStatus,tv_price;

        public ViewHolder(View itemView) {
            super(itemView);
            txtStatus=itemView.findViewById(R.id.txtStatus);
            tv_price=itemView.findViewById(R.id.tv_price);
        }
    }
}