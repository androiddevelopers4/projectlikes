package com.projectlikes;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.projectlikes.Activities.SplashActivity;
import com.projectlikes.Fragments.CartFragment;
import com.projectlikes.Fragments.HomeFragment;
import com.projectlikes.Fragments.OrderFragment;
import com.projectlikes.Fragments.ProductListFragment;
import com.projectlikes.Fragments.ProfileFragment;
import com.projectlikes.Utils.AppsContants;
import com.projectlikes.Utils.LocaleUtils;

public class MainActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener{

    String strStatus="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        strStatus = AppsContants.sharedpreferences.getString(AppsContants.OrderFragmentStatus, "");
        Log.e("erterte",strStatus);
        BottomNavigationView bottomNav = findViewById(R.id.bottom_navigation);
        bottomNav.setOnNavigationItemSelectedListener(this);




        AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        final String strUserId = AppsContants.sharedpreferences.getString(AppsContants.UserId, "");
       String strLanguages = AppsContants.sharedpreferences.getString(AppsContants.language, "");
        Log.e("hasdjkad",strLanguages);

        if (strLanguages.equals("English")) {
            LocaleUtils.setLocale(MainActivity.this, 0);
            SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
            editor.putString(AppsContants.language, "English");
            editor.commit();
        }
        else if(strLanguages.equals("Italic")) {

            LocaleUtils.setLocale(MainActivity.this, 1);
            SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
            editor.putString(AppsContants.language, "Italic");
            editor.commit();
        }
        else {

            LocaleUtils.setLocale(MainActivity.this, 0);
            SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
            editor.putString(AppsContants.language, "English");
            editor.commit();
        }


        if(strStatus.equals("1")){

            if (savedInstanceState == null) {
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new OrderFragment()).addToBackStack(null).commit();
            }
        }
        else{

            if (savedInstanceState == null) {
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new HomeFragment()).addToBackStack(null).commit();
            }
        }

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

        switch (menuItem.getItemId()) {
            case R.id.nav_home:

                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new HomeFragment()).addToBackStack(null).commit();

                break;

            case R.id.nav_profile:

                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new ProfileFragment()).addToBackStack(null).commit();

                break;
            case R.id.nav_cart:

                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new CartFragment()).addToBackStack(null).commit();

                break;

            case R.id.nav_product:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new ProductListFragment()).addToBackStack(null).commit();
                break;

            case R.id.nav_order:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new OrderFragment()).addToBackStack(null).commit();
                break;


        }

            return true;
    }
}