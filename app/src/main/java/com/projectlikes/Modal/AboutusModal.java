package com.projectlikes.Modal;

public class AboutusModal {

    public int img;
    public String title;
    public String discription ;

    public AboutusModal(int img, String title, String discription) {
        this.img = img;
        this.title = title;
        this.discription = discription;
    }

    public int getImg() {
        return img;
    }

    public void setImg(int img) {
        this.img = img;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDiscription() {
        return discription;
    }

    public void setDiscription(String discription) {
        this.discription = discription;
    }
}
