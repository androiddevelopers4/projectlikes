package com.projectlikes.Modal;

public class CartModal {

    public String img;
    public String product_name;
    public String discount_percentage;
    public String product_price;
    public String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getDiscount_percentage() {
        return discount_percentage;
    }

    public void setDiscount_percentage(String discount_percentage) {
        this.discount_percentage = discount_percentage;
    }

    public String getProduct_price() {
        return product_price;
    }

    public void setProduct_price(String product_price) {
        this.product_price = product_price;
    }

    public CartModal(String id,String img, String product_name, String discount_percentage, String product_price) {
        this.id = id;
        this.img = img;
        this.product_name = product_name;
        this.discount_percentage = discount_percentage;
        this.product_price = product_price;
    }
}
