package com.projectlikes.Modal;

public class FacilityModal {

    public String Id;
    public String Icon;
    public String title;
    public String description;
    public String it_title;
    public String it_des;
    public String path;

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getIcon() {
        return Icon;
    }

    public void setIcon(String icon) {
        Icon = icon;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIt_title() {
        return it_title;
    }

    public void setIt_title(String it_title) {
        this.it_title = it_title;
    }

    public String getIt_des() {
        return it_des;
    }

    public void setIt_des(String it_des) {
        this.it_des = it_des;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}