package com.projectlikes.Modal;

public class ProductListItemModal {
    public  int image;
    public  String Product_name;
    public  String followers_no;
    public  String price;

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getProduct_name() {
        return Product_name;
    }

    public void setProduct_name(String product_name) {
        Product_name = product_name;
    }

    public String getFollowers_no() {
        return followers_no;
    }

    public void setFollowers_no(String followers_no) {
        this.followers_no = followers_no;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPercent() {
        return percent;
    }

    public void setPercent(String percent) {
        this.percent = percent;
    }

    public String getFinal_price() {
        return final_price;
    }

    public void setFinal_price(String final_price) {
        this.final_price = final_price;
    }

    public ProductListItemModal(int image, String product_name, String followers_no, String price, String percent, String final_price) {
        this.image = image;
        Product_name = product_name;
        this.followers_no = followers_no;
        this.price = price;
        this.percent = percent;
        this.final_price = final_price;

    }

    public  String percent;




    public  String final_price;

}
