package com.projectlikes.Modal;

public class ProductListModal {


    public ProductListModal(String product_title, String discription) {
        Product_title = product_title;
        this.discription = discription;
    }


    public  String Product_title;
    public  String discription;

    public String getProduct_title() {
        return Product_title;
    }

    public void setProduct_title(String product_title) {
        Product_title = product_title;
    }

    public String getDiscription() {
        return discription;
    }

    public void setDiscription(String discription) {
        this.discription = discription;
    }





}
