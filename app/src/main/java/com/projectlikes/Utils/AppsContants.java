package com.projectlikes.Utils;

import android.content.SharedPreferences;

public class AppsContants {

    public static SharedPreferences sharedpreferences;
    private static String myprefrencesKey;
    public static final String MyPREFERENCES = myprefrencesKey;
    public static final String UserId = "UserId";
    public static final String fname = "fname";
    public static final String lname = "lname";
    public static final String email = "email";
    public static final String gender = "gender";
    public static final String phone = "phone";
    public static final String rafral_id = "rafral_id";
    public static final String image = "image";
    public static final String ProductId = "ProductId";
    public static final String ProductName = "ProductName";
    public static final String ProductCatId = "ProductCatId";
    public static final String ProductCatName = "ProductCatName";
    public static final String Dicount = "Dicount";
    public static final String Price = "Price";
    public static final String SalePrice = "SalePrice";
    public static final String Image = "Image";
    public static final String Description = "Description";
    public static final String path = "path";
    public static final String CartId = "CartId";
    public static final String Links = "Links";
    public static final String TotalPrice = "TotalPrice";
    public static final String OrderFragmentStatus = "OrderFragmentStatus";
    public static final String language = "language";


}